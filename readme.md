# Teste instruções

### Instalação Adonis

```sh
$ git clone git@bitbucket.org:ramon-oestreich/teste-zukk.git
$ cd teste-zukk/back-end/api-zukk
$ npm install
$ cp .env.example .env
$ adonis key:generate
$ configure .env com banco de dados
$ adonis migration:run
$ adonis seed --files='UserSeeder.js'
$ subir servidor  npm start
```

## Exemplo .ENV

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_USER=root
DB_PASSWORD=root
DB_DATABASE=adoniss

```

## Routes Clients

| Route                  | Handler                    | Middleware | Name             | Domain |
| ---------------------- | -------------------------- | ---------- | ---------------- | ------ |
| GET /clients           | ProductsController.index   | auth       | products.index   |        |
| POST /clients          | ProductsController.store   | auth       | products.store   |        |
| GET /clients/:id       | ProductsController.show    | auth       | products.show    |        |
| PUT,PATCH /clients/:id | ProductsController.update  | auth       | products.update  |        |
| DELETE /clients/:id    | ProductsController.destroy | auth       | products.destroy |        |

## Routes Users

| Route                | Handler                    | Middleware | Name             | Domain |
| -------------------- | -------------------------- | ---------- | ---------------- | ------ |
| GET /users           | ProductsController.index   | auth       | products.index   |        |
| POST /users          | ProductsController.store   | auth       | products.store   |        |
| GET /users/:id       | ProductsController.show    | auth       | products.show    |        |
| PUT,PATCH /users/:id | ProductsController.update  | auth       | products.update  |        |
| DELETE /users/:id    | ProductsController.destroy | auth       | products.destroy |        |

## Routes Login

| Route       | Handler              | Middleware | Name   | Domain |
| ----------- | -------------------- | ---------- | ------ | ------ |
| POST /login | UserController.login |            | /login |        |

### Front-end angular

```sh
$ git clone git@bitbucket.org:ramon-oestreich/teste-zukk.git
$ cd cd front-end/app-zukk/
$ npm install
$ subir servidor  npm start
$ login  email: admin@admin.com | password: 1234
```
