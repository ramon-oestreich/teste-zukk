import { Component, OnInit } from '@angular/core';
import {Client} from "../_models/client";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router, ActivatedRoute} from "@angular/router";
import {ClientService} from "../_services/client.service";
import {first} from "rxjs/operators";

@Component({
  selector: 'app-client-edit',
  templateUrl: './client-edit.component.html',
  styleUrls: ['./client-edit.component.css']
})
export class ClientEditComponent implements OnInit {

  client: Client;
  editForm: FormGroup;
  current = null;
  constructor(private formBuilder: FormBuilder,private router: Router, private clientService: ClientService,private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.editForm = this.formBuilder.group({
      id: [''],
      nome: ['', Validators.required],
      endereco: ['', Validators.required],
      bairro: ['', Validators.required],
      cidade: ['', Validators.required],
      uf: ['', Validators.required],
      telefone: ['', Validators.required],
      email: ['', Validators.required]
    });

    this.getClient(this.route.snapshot.paramMap.get('id'));
  }

  getClient(id): void {
    this.clientService.find(id)
      .subscribe(
        product => {
          this.current = product;
          this.editForm.setValue(this.current);
          console.log(product);
        },
        error => {
          console.log(error);
        });
  }

  onSubmit() {
    this.clientService.update(this.current.id,this.editForm.value)
      .pipe(first())
      .subscribe(
        data => {
            this.router.navigate(['clients']);
        },
        error => {
          alert(error);
        });
  }
}
