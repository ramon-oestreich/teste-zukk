export class Client {
    id: number;
    nome: string;
    email: string;
    endereco: string;
    bairro: string;
    cidade: string;
    uf: string;
    telefone: string;
}