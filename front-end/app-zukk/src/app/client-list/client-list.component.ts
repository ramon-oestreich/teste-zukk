import { Client } from './../_models/client';
import { ClientService } from './../_services/client.service';
import { Router } from '@angular/router';
import { AfterViewInit, Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.css']
})
export class ClientListComponent implements AfterViewInit {

  clients: Client[];

  constructor(private router: Router, private clientService: ClientService) { }

  ngOnInit(): void {




  }
  ngAfterViewInit() {
    this.clientService.getAll()
      .subscribe(data => {
        this.clients = data;
      });
  }

  deleteClient(client: Client): void {
    console.log(client.id)
    this.clientService.delete(client.id)
      .subscribe(data => {
        this.clients = this.clients.filter(c => c !== client);
      })
  }

  editClient(client: Client): void {
    this.router.navigate(['edit-client']);
  };


  addClient(): void {
    this.router.navigate(['add-client']);
  };
}
