import { Observable } from 'rxjs';
import { environment } from './../../environments/environment';
import { Client } from './../_models/client';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private http: HttpClient) { }


  getAll(): Observable<Client[]> {
    return this.http.get<Client[]>(`${environment.apiUrl}/clients`);
  }

  create(client: Client): Observable<Client> {
    return this.http.post<Client>(`${environment.apiUrl}/clients`, client);
  }

  delete(id: number): Observable<Client> {
    return this.http.delete<Client>(`${environment.apiUrl}/clients/${id}`);
  }

  find(id: number): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}/clients/${id}`);
  }

  update(id, data): Observable<Client> {
    return this.http.put<Client>(`${environment.apiUrl}/clients/${id}`, data);
  }

}
