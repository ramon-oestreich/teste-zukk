'use strict'


/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.resource('users', 'UserController').middleware(['auth']);
Route.resource('clients','ClientController').middleware(['auth']);

Route.post('/login','UserController.login');
