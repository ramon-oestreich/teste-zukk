'use strict'
const Client = use('App/Models/Client');
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with clients
 */


class ClientController {


  /**
   * Show a list of all clients.
   * GET clients
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async index ({ request, response }) {
    return await Client.all()
  }



  /**
   * Create/save a new client.
   * POST clients
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const data = request.all();
    if (data){
      const client = await Client.create(data);

      return client;
    }
    return response.status(500);

  }

  /**
   * Display a single client.
   * GET clients/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {

    const client = await Client.find(params);
    return client;
  }



  /**
   * Update client details.
   * PUT or PATCH clients/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const client = await Client.findOrFail(params.id);
    const {nome,endereco, bairro, cidade, uf , telefone, email} = request.all();

    client.nome =  nome;
    client.endereco = endereco;
    client.bairro =  bairro;
    client.cidade =  cidade;
    client.uf =  uf;
    client.telefone =  telefone;
    client.email =  email;

    await client.save();
  }

  /**
   * Delete a client with id.
   * DELETE clients/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    const user = await Client.findOrFail(params.id)
    await user.delete()
    return 200;
  }
}

module.exports = ClientController
