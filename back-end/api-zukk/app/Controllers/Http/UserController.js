'use strict'
const User = use('App/Models/User');
class UserController {

  async index ({ request, response }) {
    return await User.all()
  }

  async store ({ request, response }) {
    const data = request.only([
      'username',
      'password',
      'email'
    ]);

    const user =  await User.create(data);

    return user;
  }


  async login ({ auth, request }) {
    const { email, password } = request.all()
    const token = await auth.attempt(email, password)

    return token


  }





}

module.exports = UserController
