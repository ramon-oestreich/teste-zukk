'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ClientsSchema extends Schema {
  up () {
    this.create('clients', (table) => {
      table.increments()
      table.string('nome', 80).notNullable()
      table.string('endereco', 254).notNullable()
      table.string('bairro', 254).notNullable()
      table.string('cidade', 254).notNullable()
      table.string('uf', 3).notNullable()
      table.string('telefone', 15).notNullable().unique()
      table.string('email', 254).notNullable().unique()
      table.timestamps()
    })
  }

  down () {
    this.drop('clients')
  }
}

module.exports = ClientsSchema
