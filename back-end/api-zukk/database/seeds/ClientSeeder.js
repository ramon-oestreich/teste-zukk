'use strict'


/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

class ClientSeeder {
  async run () {
    await Factory.model('App/Models/Client').create();
  }
}

module.exports = ClientSeeder
