'use strict'

const Hash = use('Hash')
const User = use('App/Models/User')
/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

class UserSeeder {
  async run () {
    await User.create({
      username: 'admin',
      email: 'admin@admin.com',
      password: await Hash.make('1234')
    });
  }
}

module.exports = UserSeeder
